variable private_key_path {
    description = "private_key"
}

variable "zone_id" {
    default  = "secret"
    description = "zone-id"
}

variable "email" {
    default     = "secret" 
    description = "email"  
}

variable "api_key" {
    default = "secret"  
    description = "api"
}

