
resource "google_compute_disk" "disk-1" {
  name = "disk-5"
  type = "pd-standard"
  size = 10
  zone = "europe-west4-a"
}

resource "google_compute_disk" "disk-2" {
  name = "disk-7"
  type = "pd-standard"
  size = 8
  zone = "europe-west4-a"
}


resource "google_compute_address" "my-static-ip" {
  name = "ipv4-addr"
}

data "google_compute_image" "debian" {
  family = "debian-10"
  project = "debian-cloud"
}

resource "google_compute_instance" "instance-with-ip" {
  name                         = "vm-instance"
  machine_type                 = "e2-small"
  metadata_startup_script      = file("user_data.sh")
  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.my-static-ip.address
    }
  }
  boot_disk {
    initialize_params {
      image = data.google_compute_image.debian.self_link
    }
  }
  
  provisioner "remote-exec" {
    connection {
      user             = "karinakitty555"
      host             = google_compute_address.my-static-ip.address
      private_key      = "${file(var.private_key_path)}"
    }
    inline = [  
      "sudo sh -c 'echo Juneway-test ${self.name} ${google_compute_address.my-static-ip.address} > /home/karinakitty555/html/index.html'"  
    ]
  }
}

resource "google_compute_attached_disk" "attach-1" {
  disk       = google_compute_disk.disk-1.id
  instance   = google_compute_instance.instance-with-ip.id
}

resource "google_compute_attached_disk" "attach-2" {
  disk       = google_compute_disk.disk-2.id
  instance   = google_compute_instance.instance-with-ip.id
}

resource "cloudflare_record" "juneway" {
  zone_id = var.zone_id
  name    = "alex-slv.juneway.pro" 
  value   = google_compute_address.my-static-ip.address 
  type    = "A"
  ttl     = 3600
}


