
resource "google_compute_firewall" "fw" {
    name     = "http-2"
    network  = "default"
    source_ranges  = ["0.0.0.0/0"]
  dynamic "allow" {
    for_each = ["80", "8001", "8802"]
    content {
        protocol       = "tcp"
        ports          = allow.value[*]
        
    }

  }
}