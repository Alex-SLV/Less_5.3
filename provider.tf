provider "google" {  
  project = "prod-22"
  region  = "europe-west4"
  zone    = "europe-west4-a"

}

terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "cloudflare" {
  email   = var.email
  api_key = var.api_key
}